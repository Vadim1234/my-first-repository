package ru.abg.bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DateBaseSel {

	//v1.1
	
	public static void main(String[] args) {
		Connection con = null;
		ResultSet rt = null;
		Statement stm = null;
		
		try {
			
			//Connection to dateBase
			
			con = DriverManager.getConnection("jdbc:mysql://localhost/my_bd","Chel","1234");
			
			//sql statement
			
			String sql = "SELECT * FROM produckts";
			
			stm = con.createStatement();
			
			//execute sql
			
			rt = stm.executeQuery(sql);
			
			
			//browse bd and print her to console
			
			while(rt.next()){
				
				System.out.println(rt.getString("id" )+"  "+rt.getString("produckt") + " " + rt.getString("value") + " " + rt.getString("kol"));
			}
		} catch (SQLException e) {			
			e.printStackTrace();
		}finally{
			
			//if dont empty con, rt, stm to close 
			
				try {
					if(con != null)	con.close();
					if(rt != null)rt.close();
					if(stm != null)stm.close();
				} catch (SQLException e) {					
					e.printStackTrace();
				}
			
		}

	}

}
