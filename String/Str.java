package ru.abg.str;

public class Str {

	public static void main(String[] args) {
		
		//String 
		
		String str = "5455455-16554-5464";
		
		//number string 
		
		int col = str.length()-9;
		
		//line separation "-" regulator
		
		String[] in = str.split("-");
		
		//detection symbol in str
		
		char ae = str.charAt(col);
		
		//detection symbols
		
		boolean con = str.contains("16554");
		
		//findings
		
		System.out.println(con);
		
		System.out.println();
		
		System.out.println(in[0]);
		System.out.println(in[1]);
		System.out.println(in[2]);
		
		System.out.println();
		
		System.out.println(str.toUpperCase());
		
		System.out.println();
		
		System.out.println(ae);
		
		

	}

}
