package extends1;

//create runtime class

public class runtime {

	public static void main(String[] args) {		
		NoteBook note = new NoteBook();
		
		//This begins with Polymorphism
		//announcing the type of NoteBook variabel we can assign it to any child object
		
		NoteBook  planshHP = new planshHP();
				
		NoteBook planshExplore = new PlanshExplore();
		
		//param variable child class
		
		 planshHP.name="HP plansh";
		 planshHP.wes=1.5;		
		 planshHP.OP="Linux";
		 
		 planshExplore.name="Explore";
		 planshExplore.wes=1.1;
		 planshExplore.OP="Linux";
		
		 //call method child class
		 
		 ((PlanshExplore) planshExplore).keyboardUSB();
		 
		//param variable parent class
		 
		note.name="HP";
		note.OP="Windws";
		note.wes=2.8;
		
		//method is used as a template 
		//and as a result displays the result of child classes methods with their parameters
		
		speekOP(planshHP);
		
		speekOP(planshExplore);
		
		speekOP(note);		

	}
	
	//here is a basic principle of polymorphism
	
	//we move to the parameter variable type of the parent object and pass 
	//through the point methods of the parent object
	
	public static void  speekOP(NoteBook notebook){
		notebook.name();
		notebook.OP();
		notebook.wes();
	}

}
