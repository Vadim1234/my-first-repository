package com.javarush.test.level07.lesson06.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/* Самая длинная строка
1. Создай список строк.
2. Считай с клавиатуры 5 строк и добавь в список.
3. Используя цикл, найди самую длинную строку в списке.
4. Выведи найденную строку на экран.
5. Если таких строк несколько, выведи каждую с новой строки.
*/
public class Solution
{
    public static void main(String[] args) throws Exception
    {

        //create scanner
        Scanner scn = new Scanner(System.in);

        //create list from strings

        ArrayList<String> list = new ArrayList<String>();

        //cycle for filling list string from console

        for(int i = 0; i<5; i++){
            list.add(scn.next());
        }

        //create variable for finding very long line

        int max = list.get(0).length();

        // find very long line

        for(int i =0; i < list.size(); i++){
            if(max<list.get(i).length()){
                max = list.get(i).length();
            }
        }
        //output line on console

        for(int i =0; i<list.size(); i++){
            if(max == list.get(i).length()){
                System.out.println(list.get(i));
            }
        }

    }
}
